public class Segitiga1 extends SegitigaTemplate{

    public String getTitle(){
        return "Segitiga Siku 10 Baris";
    }

    public int getJumlahBaris(){
        return 10;
    }

    public String getCharacter(){
        return "*";
    }
}
