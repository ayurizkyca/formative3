public abstract class SegitigaTemplate {

    public final void buat(){
        int jumlah = getJumlahBaris();
        System.out.println(getTitle());

        for (var i = jumlah; i >= 1; i--) {
            for (var j = 1; j <= jumlah; j++) {
                if (j >= i) {
                    System.out.print(getCharacter());
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

    }

    public abstract String getTitle();

    public abstract int getJumlahBaris();

    public abstract String getCharacter();
}
