public class Segitiga2 extends SegitigaTemplate{
    
    public String getTitle(){
        return "Segitiga Siku 25 Baris";
    }

    public int getJumlahBaris(){
        return 25;
    }

    public String getCharacter(){
        return "#";
    }
}
