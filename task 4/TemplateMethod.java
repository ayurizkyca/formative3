/* TASK 4
 * Membuat class TemplateSegitiga dimana didalamnya terdapat method buat
 * sebagai template pembuatan segitiga
 * Class TemplateSegitiga ini dapat digunakan jika ingin membuat segitiga baru
 * yaitu dengan mengextends template
 * 
 * logic yang dilakukan sama yaitu membuat segitiga siku siku,
 * namun yang dibedakan adalah title, jumlah baris segitiga, dan character yang digunakan
 * 
 */

public class TemplateMethod {
    public static void main(String[] args) {
        Segitiga1 segitigaKecil = new Segitiga1();
        segitigaKecil.buat();

        Segitiga2 segitigaBesar = new Segitiga2();
        segitigaBesar.buat();
    }
}
