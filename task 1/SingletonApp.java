/*Singleton pattern memungkinkan kita membuat 1 object dalam 1 aplikasi
 * dimana jika kita memerlukan object tersebut dan ditempatkan dibagian code manapun
 * akan mengembalikan object yang sama
 * 
 * Singleton pattern digunakan jika ada problem dengan object yang
 * dibuat memakan cost yang lumayan, contohnya koneksi database
 * 
 * jika kita memanggil koneksi database disetiap saat menyimpan data (dalam kode ini menggunakan method save)
 * maka sebaiknya koneksi database dibuatkan object tersendiri (DatabaseHelper)
 * dan jika koneksi tersebut dibutuhkan, tinggal memanggi object yang telah dibuat
 * tanpa menuliskan ulang koneksi database
 */

public class SingletonApp {
    
    public static void main(String[] args) {
        User user = new User();
        user.save("U0001", "Yuyu");

        Order order = new Order();
        order.save("B001", "Pasta Gigi", 1);
        order.save("B001", "Sikat Gigi", 3);
        order.save("B001", "Sabun Mandi", 5);
    }
}
